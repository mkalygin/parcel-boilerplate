module.exports = {
  extends: [
    'airbnb-base',
    'plugin:react/recommended',
  ],

  env: {
    browser: true,
    es6: true,
    jquery: true,
  },

  plugins: [
    'react',
  ],

  parserOptions: {
    ecmaFeatures: {
      modules: true,
      jsx: true,
    },
  },

  settings: {
    react: {
      version: '16.4.2',
    },
  },

  rules: {
    'no-mixed-operators': 'off',
    'global-require': 'off',
    'class-methods-use-this': 'off',
    'import/no-extraneous-dependencies': 'off',
    'react/prop-types': 'off',
    'react/no-unknown-property': 'off',
  },
};
